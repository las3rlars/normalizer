#include <stdio.h>
#include <stdlib.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#endif

#include "glm/glm/ext/vector_int2.hpp"
#include "glm/glm/vec3.hpp"
#include "glm/glm/vec2.hpp"
#include "glm/glm/gtc/constants.hpp"
#include "glm/glm/geometric.hpp"

#include <cmath>
#include <cstdlib>
#include <cwchar>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


glm::vec3 lngLatToWorld(glm::dvec2 lngLat)
{
    float y = std::sin(lngLat.y);
    float r = std::cos(lngLat.y);
    float x = std::sin(lngLat.x) * r;
    float z = -std::cos(lngLat.x) * r;
    return glm::vec3(x, y, z);
}

uint16_t getPixel(const uint16_t* texture, glm::ivec2 texCoord, int textureWidth, int textureHeight)
{
    return texture[(texCoord.y * textureWidth) + texCoord.x]; // MAD?
}

glm::dvec3 calculateWorldPoint(const uint16_t* texture, glm::ivec2 texCoord, int textureWidth, int textureHeight)
{
    texCoord.x = (texCoord.x + textureWidth) % textureWidth;
    texCoord.y = (texCoord.y + textureHeight) % textureHeight;

    glm::dvec2 uv = glm::dvec2(texCoord) / glm::dvec2(textureWidth - 1, textureHeight - 1);
    glm::dvec2 lngLat = (uv - 0.5) * glm::pi<double>() * glm::dvec2(2.0, 1.0);

    glm::dvec3 spherePoint = lngLatToWorld(lngLat);
    uint16_t height = getPixel(texture, texCoord, textureWidth, textureHeight);
    double worldHeight = 6378160.0 + height * 8.0;
    return spherePoint * worldHeight;
}

glm::vec3 generateNormal(const uint16_t *texture, glm::ivec2 texCoord, int textureWidth, int textureHeight)
{
    glm::dvec3 posNorth = calculateWorldPoint(texture, texCoord + glm::ivec2(0, 1), textureWidth, textureHeight);
    glm::dvec3 posSouth = calculateWorldPoint(texture, texCoord + glm::ivec2(0, -1), textureWidth, textureHeight);
    glm::dvec3 posEast = calculateWorldPoint(texture, texCoord + glm::ivec2(1, 0), textureWidth, textureHeight);
    glm::dvec3 posWest = calculateWorldPoint(texture, texCoord + glm::ivec2(-1, 0), textureWidth, textureHeight);

    glm::dvec3 dirNorth = glm::normalize(posNorth - posSouth);
    glm::dvec3 dirEast = glm::normalize(posEast - posWest);
    glm::vec3 normalVector = glm::normalize(glm::cross(dirNorth, dirEast));
    return normalVector;
}

int main(int argc, const char* argv[])
{

    if (argc < 5) {
        fprintf(stderr, "not enough arguments\n");
        return EXIT_FAILURE;
    }

    int width = std::atoi(argv[2]);
    int height = std::atoi(argv[3]);

#ifndef _WIN32
    int fdin;
    if ((fdin = open(argv[1], O_RDWR)) < 0) {
        fprintf(stderr, "could not open file\n");
        return EXIT_FAILURE;
    }

    struct stat sbuf;
    if (fstat(fdin, &sbuf) < 0) {
        fprintf(stderr, "could not stat file\n");
        return EXIT_FAILURE;
    }

    uint16_t* src = 0;
    //void* src = 0;

    printf("file size: %lld\n", sbuf.st_size);
    if ((src = (uint16_t*)mmap(NULL, sbuf.st_size, PROT_READ, MAP_PRIVATE, fdin, 0)) == MAP_FAILED) {
        fprintf(stderr, "could not mmap file\n");
        return EXIT_FAILURE;
    }
#else
    auto state = std::mbstate_t();
    auto fileNameLength = std::mbsrtowcs(nullptr, &argv[1], 0, &state);
    auto* buf = new wchar_t[fileNameLength + 1]();
    auto temp = mbsrtowcs(buf, &argv[1], fileNameLength + 1, &state);


    HANDLE file = CreateFile(buf, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if (file == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "could not open file\n");
        return EXIT_FAILURE;
    }

    LARGE_INTEGER size;
    if (!GetFileSizeEx(file, &size)) {
        fprintf(stderr, "could not get file size\n");
        CloseHandle(file);
        return EXIT_FAILURE;
    }

    HANDLE mapping = CreateFileMapping(file, 0, PAGE_READONLY, 0, 0, 0);
    if (mapping == 0) {
        fprintf(stderr, "could not create file mapping\n");
        CloseHandle(file);
        return EXIT_FAILURE;
    }

    const uint16_t* src = (const uint16_t*)MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, 0);
    if (!src) {
        fprintf(stderr, "could not create map view of file\n");
        CloseHandle(mapping);
        CloseHandle(file);
        return EXIT_FAILURE;
    }
#endif

    int stride = width * 3;
    uint8_t* dst = new uint8_t[stride*height];

    int offset = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            glm::vec3 normal = generateNormal(src, glm::ivec2(x, y), width, height) * 0.5f + 0.5f;
            //int offset = y * stride + (x*3);
            dst[offset+0] = normal.x * 255;
            dst[offset+1] = normal.y * 255;
            dst[offset+2] = normal.z * 255;
            offset += 3;
        }
    }

    stbi_write_png(argv[4], width, height, 3, dst, stride);

    delete[] dst;

#ifndef _WIN32
    munmap(src, sbuf.st_size);
#else
    UnmapViewOfFile(src);
    CloseHandle(mapping);
    CloseHandle(file);
#endif

    return EXIT_SUCCESS;
}